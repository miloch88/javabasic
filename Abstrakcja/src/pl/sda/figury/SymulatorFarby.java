package pl.sda.figury;

public class SymulatorFarby {

    public static int obliczZapotrzebowanieNaFarbe(Figura[] figury, double pojemnosc) {
        double powerzchniaCalkowita = 0;

        for (Figura figura : figury) {
            powerzchniaCalkowita += figura.obliczPole();
        }

        if (powerzchniaCalkowita / pojemnosc == 0) {
            return (int) (powerzchniaCalkowita / pojemnosc);
        } else
            return (int) (powerzchniaCalkowita / pojemnosc) + 1;
    }

    public static void main(String[] args) {

        Figura kwadrat = new Kwadrat(4);
        Figura protokat = new Prostokat(2, 5);
        Figura kolo = new Okreg(6);

        Figura[] figury = new Figura[]{kwadrat, protokat, kolo};

        double pojemnosc = 50;

        System.out.println(obliczZapotrzebowanieNaFarbe(figury, pojemnosc));
    }
}
