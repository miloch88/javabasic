package pl.sda.bilety;

public enum  Bilet {
    ULGOWY_GODZINNY(1.6, 60),
    ULGOWY_CALODNIOWY(5.2, 24*60),
    NORMALY_GODZINNY(3.2, 60),
    NORMALNY_CALODNIOWY(10.40, 24*60),
    BRAK_BILETU(0.00, 0);

    private double cena;
    private int czasWMinutach;

    Bilet(double cena, int czasWMinutach) {
        this.cena = cena;
        this.czasWMinutach = czasWMinutach;
    }

    public double pobierzCeneBiletu(){
        return cena;
    }

    public int pobierzCzasJazdy(){
        return czasWMinutach;
    }

    public void wyswietlDaneOBilecie(){
        String nazwa = this.toString();//ULGOWY_GODZINNY
        nazwa = nazwa.toLowerCase();//ulgowy_godzinny
        nazwa = nazwa.split("_")[0];//ulgowy
        System.out.println(String.format(
                "Bilet %s %d-godzinny", nazwa, czasWMinutach/60
        ));
    }

    public static Bilet kupBilet(int wiek, int czasWMinutach, double kotwa){
        Bilet bilet = Bilet.BRAK_BILETU;
        if(wiek<=18 || wiek>65){
            if(czasWMinutach>=60){
                bilet = Bilet.ULGOWY_CALODNIOWY;
            }else{
                bilet = Bilet.ULGOWY_GODZINNY;
            }
        }else if(czasWMinutach>=60){
            bilet = Bilet.NORMALNY_CALODNIOWY;
        }else{
            bilet = Bilet.NORMALY_GODZINNY;
        }


        if(bilet.pobierzCeneBiletu()<=kotwa){
            return bilet;
        }
        return Bilet.BRAK_BILETU;

    }
}
