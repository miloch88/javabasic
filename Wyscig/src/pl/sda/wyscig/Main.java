package pl.sda.wyscig;

public class Main {
    public static void main(String[] args) {

        Zawodnik zawodnik1 = new Zawodnik("Maciek", 01, 10, 15);
        Zawodnik zawodnik2 = new Zawodnik("Kasia", 02, 10, 15);
        Zawodnik zawodnik3 = new Zawodnik("Bartek", 03, 10, 15);

        Zawodnik[] zawodnicy = new Zawodnik[]{zawodnik1, zawodnik2, zawodnik3};

        Zawodnik zwyciezca = null;

        do {
            for (Zawodnik zawodnik : zawodnicy) {
                zawodnik.biegnij();
                if (zawodnik.getPokonanaPredkosc() >= 1000) {
                    zwyciezca = zawodnik;
                    break;
                }
            }
        } while (zwyciezca == null);

        System.out.println("Zwycięzył " + zwyciezca.imie);
        zwyciezca.przedstawSie();

    }
}




