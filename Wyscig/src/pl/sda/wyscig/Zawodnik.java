package pl.sda.wyscig;

import java.util.Random;

public class Zawodnik {
    String imie;
    private int nrStarowy;
    private int predkoscMinimalna;
    private int predkoscMaksymalna;
    private int odleglosc = 0;

    public Zawodnik(String imie, int nrStarowy, int predkoscMinimalna, int predkoscMaksymalna) {
        this.imie = imie;
        this.nrStarowy = nrStarowy;
        this.predkoscMinimalna = predkoscMinimalna;
        this.predkoscMaksymalna = predkoscMaksymalna;
    }

    public int getPokonanaPredkosc(){
        return odleglosc;
    }

    void przedstawSie() {
        System.out.printf("Nazywam się %s, mam numer #%d, biegam z prędkością od %d km/h do do %d km/h\n", imie, nrStarowy, predkoscMinimalna, predkoscMaksymalna);
    }

    public void biegnij() {
        Random random = new Random();
        odleglosc += random.nextInt(predkoscMaksymalna - predkoscMinimalna) + predkoscMinimalna;
    }
}




