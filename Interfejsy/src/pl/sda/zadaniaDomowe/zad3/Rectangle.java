package pl.sda.zadaniaDomowe.zad3;

public class Rectangle implements IShape {

    double a;
    double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculateArea() {
        return a*b;
    }

    @Override
    public double calculateCircumference() {
        return 2*(a+b);
    }

    @Override
    public void paint() {
        for (int i = 0; i < a ; i++) {
            for (int j = 0; j < b ; j++) {
                System.out.print("*");
            }
            System.out.println();

        }
    }
}
