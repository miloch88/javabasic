package pl.sda.zadaniaDomowe.zad3;

public interface IShape {

    double pi = 3.1415;

    double calculateArea();
    double calculateCircumference();
    void paint();

}
