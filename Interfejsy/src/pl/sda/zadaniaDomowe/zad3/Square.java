package pl.sda.zadaniaDomowe.zad3;

public class Square implements IShape {

    double a;

    @Override
    public double calculateArea() {
        return a*a;
    }

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double calculateCircumference() {
        return 4*a;
    }

    @Override
    public void paint() {
        for (int i = 0; i < a ; i++) {
            for (int j = 0; j < a ; j++) {
                System.out.print("*");
            }
            System.out.println();

        }
    }
}
