package pl.sda.zadaniaDomowe.zad3;

public class Circle implements IShape {

    public Circle(double r) {
        this.r = r;
    }

    double r;

    @Override
    public double calculateArea() {
        return pi*r*r;
    }

    @Override
    public double calculateCircumference() {
        return 2*pi*r;
    }

    @Override
    public void paint() {
       int n;
        if (r > (int) r) {
            n = (int) r + 1;
        } else {
            n = (int) r;
        }

        for (int i = -n; i <= n; i++) {
            for (int j = -n; j <= n; j++) {
                if (i * i + j * j <= n * n) System.out.print("* ");
                else System.out.print("  ");
            }
            System.out.println();
        }
    }
}
