package pl.sda.zadaniaDomowe.zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String wybor;
        double r, a;

        do{
            System.out.println("Podaj swoją figurę (circle/square/rectangle):");
            wybor = scanner.nextLine();

            switch(wybor){
                case "circle":
                    System.out.println("Podaj swój promień");
                    r = podajLiczbe();
                    System.out.println("Co chcesz obliczyć (obwod/pole): ");
                    Circle circle = new Circle(r);
                    wybor = scanner.nextLine();
                    switch (wybor){
                        case "obwod":
                            System.out.println("Obwód: " + circle.calculateCircumference());
                            break;
                        case "pole":
                            System.out.println("Pole: " + circle.calculateArea());
                            break;
                    }
                    circle.paint();
                    break;
                case "square":
                    System.out.println("Podaj swój bok: ");
                    r = podajLiczbe();
                    System.out.println("Co chcesz obliczyć (obwod/pole): ");
                    Square square = new Square(r);
                    wybor = scanner.nextLine();
                    switch (wybor){
                        case "obwod":
                            System.out.println("Obwód: " + square.calculateCircumference());
                            break;
                        case "pole":
                            System.out.println("Pole: " + square.calculateArea());
                            break;
                    }
                    square.paint();
                    break;
                case "rectangle":
                    System.out.println("Podaj swoje boki: ");
                    r = podajLiczbe();
                    a = podajLiczbe();
                    System.out.println("Co chcesz obliczyć (obwod/pole): ");
                    Rectangle rectangle = new Rectangle(r,a);
                    wybor = scanner.nextLine();
                    switch (wybor){
                        case "obwod":
                            System.out.println("Obwód: " + rectangle.calculateCircumference());
                            break;
                        case "pole":
                            System.out.println("Pole: " + rectangle.calculateArea());
                            break;
                    }
                    rectangle.paint();

            }

        }while (wybor.toLowerCase().equals("zamknij"));
    }

    private static double podajLiczbe() {
        Scanner scanner = new Scanner(System.in);
        double r = scanner.nextDouble();
        return r;
    }
}
