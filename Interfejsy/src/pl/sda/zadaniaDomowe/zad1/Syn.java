package pl.sda.zadaniaDomowe.zad1;

public class Syn implements CzlonekRodziny {

    public String getImie() {
        return imie;
    }

    private String imie;

    public Syn(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println(imie +": Who's asking?");
    }

    @Override
    public boolean CzyJestemDorosly() {
        return false;
    }
}
