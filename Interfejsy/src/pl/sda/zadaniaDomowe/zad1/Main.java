package pl.sda.zadaniaDomowe.zad1;

public class Main {
    public static void main(String[] args) {

        Ojciec ojciec = new Ojciec("Darek");
        Matka matka = new Matka("Justyna");
        Corka corka = new Corka("Ania");
        Syn syn = new Syn("Wiktor");

        CzlonekRodziny[] czlonkowieRodziny = new CzlonekRodziny[]{ojciec, matka, corka, syn};

        for (CzlonekRodziny cz : czlonkowieRodziny) {
            cz.przedstawSie();
            System.out.println(cz.CzyJestemDorosly());
        }
    }
}
