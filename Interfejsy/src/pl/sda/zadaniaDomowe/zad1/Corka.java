package pl.sda.zadaniaDomowe.zad1;

public class Corka implements CzlonekRodziny {

    public String getImie() {
        return imie;
    }

    private String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println(imie +": I am daughter");
    }

    @Override
    public boolean CzyJestemDorosly() {
        return false;
    }
}
