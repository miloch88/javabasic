package pl.sda.zadaniaDomowe.zad1;

public class Matka implements CzlonekRodziny {

    private String imie;

    public String getImie() {
        return imie;
    }

    public Matka(String imie) {
        this.imie = imie;
    }

//    @Override
//    public void przedstawSie() {
//        System.out.println(imie + ": I am mother. ");
//    }

    @Override
    public boolean CzyJestemDorosly() {
        return true;
    }
}
