package pl.sda.zadaniaDomowe.zad1;

public class Ojciec implements CzlonekRodziny {

    public String getImie() {
        return imie;
    }

    private String imie;

    public Ojciec(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println(imie +": I am father.");
    }

    @Override
    public boolean CzyJestemDorosly() {
        return true;
    }
}
