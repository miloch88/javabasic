package pl.sda.zadaniaDomowe.zad1;

public interface CzlonekRodziny {


    default void przedstawSie() {
        System.out.println("I am just a simple family member");
    }

    boolean CzyJestemDorosly();

}
