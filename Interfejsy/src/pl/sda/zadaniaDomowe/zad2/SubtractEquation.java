package pl.sda.zadaniaDomowe.zad2;

public class SubtractEquation implements ICalculable{
    public SubtractEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    double a;
    double b;

    @Override
    public double calculate() {
        return a-b;
    }
}
