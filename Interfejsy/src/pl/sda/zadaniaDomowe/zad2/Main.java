package pl.sda.zadaniaDomowe.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String wybor = null;
        double a, b;

        do {
            try {

                System.out.println("Podaj swoją komendę: (add/sub/div/mul)");
                wybor = scanner.nextLine();

                switch (wybor) {
                    case "add":
                        System.out.println("Podaj swoje dwie liczby: ");
                        a = podajLiczbeA();
                        b = podajLiczbeB();
                        SumEquation sum = new SumEquation(a, b);
                        System.out.println(sum.calculate());
                    case "sub":
                        System.out.println("Podaj swoje dwie liczby: ");
                        a = podajLiczbeA();
                        b = podajLiczbeB();
                        SubtractEquation sub = new SubtractEquation(a, b);
                        System.out.println(sub.calculate());
                    case "div":
                        System.out.println("Podaj swoje dwie liczby: ");
                        a = podajLiczbeA();
                        b = podajLiczbeB();
                        DivEquation div = new DivEquation(a, b);
                        System.out.println(div.calculate());
                    case "mul":
                        System.out.println("Podaj swoje dwie liczby: ");
                        a = podajLiczbeA();
                        b = podajLiczbeB();
                        MulEquation mul = new MulEquation(a, b);
                        System.out.println(mul.calculate());
                }

            } catch (Exception e) {
                System.out.println("Nie leć w kulki");
            }

            System.out.println("Aby zakończyć wpisz (zamknij) albo wpisz kolejną komendę (add/sub/div/mul)");

        } while (!wybor.equals("zamknij"));
    }

    private static double podajLiczbeB() {
        Scanner scanner = new Scanner(System.in);
        double b = scanner.nextDouble();
        return b;
    }

    private static double podajLiczbeA() {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        return a;
    }
}
