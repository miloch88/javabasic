package pl.sda.zadaniaDomowe.zad2;

public interface ICalculable {

    public double calculate();

}
