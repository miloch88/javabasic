package pl.sda.zadaniaDomowe.zad2;

public class MulEquation implements ICalculable {

    double a;
    double b;

    public MulEquation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        return a*b;
    }
}
