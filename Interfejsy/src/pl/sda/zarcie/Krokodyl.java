package pl.sda.zarcie;

public class Krokodyl implements Jedzacy {

    private int ileZjadzlMiesa;
    private int iloscPosilkow;


    @Override
    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() == TypPokarmu.MIESO) {
            ileZjadzlMiesa += pokarm.getWaga();
//            System.out.println("Zjadłem już: " + ileZjadzlMiesa + " mięsa");
            iloscPosilkow++;
        } else {
            System.out.println("Fuj!!! Nie jem tego!!! ");
        }
    }

    @Override
    public int ilePosilkowZjedzonych() {
        System.out.println("Zjadłem już: " + iloscPosilkow + " posiłków");
        return iloscPosilkow;
    }

    @Override
    public int ileGramowZjedzonych() {
        System.out.println("Zjadłem już: " + ileZjadzlMiesa + "g mięsa");
        return ileZjadzlMiesa;
    }
}
