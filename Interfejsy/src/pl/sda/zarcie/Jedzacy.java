package pl.sda.zarcie;

public interface Jedzacy {

    void jedz(Pokarm pokarm);
    int ilePosilkowZjedzonych();
    int ileGramowZjedzonych();
}
