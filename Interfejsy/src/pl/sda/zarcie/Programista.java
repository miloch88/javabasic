package pl.sda.zarcie;

public class Programista implements Jedzacy{
    private int ileZjadzlOwocow;
    private int ileZjadzlMiesa;
    private int iloscPosilkow;


    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() == TypPokarmu.OWOCE) {
            ileZjadzlOwocow += pokarm.getWaga();
//            System.out.println("Zjadłem już: " + ileZjadzlMiesa + " mięsa");
            iloscPosilkow++;
        } else {
            ileZjadzlMiesa += pokarm.getWaga();
            iloscPosilkow++;
        }
    }

    public int ilePosilkowZjedzonych() {
        System.out.println("Zjadłem już: " + iloscPosilkow + " posiłków");
        return iloscPosilkow;
    }


    public int ileGramowZjedzonych() {
        System.out.println("Zjadłem już: " + ileZjadzlOwocow + "g owoców");
        System.out.println("Zjadłem już: " + ileZjadzlMiesa + "g miesa");
        return 0;
    }
}
