package pl.sda.dzwon;

import java.util.Random;

public class Telefon implements Dzwoni {

    private String numerTelefonu = "1234567891111";
    private int lacznyCzasRozmow = 0;

    public Telefon(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;

    }


    @Override
    public void zadwon(String numerOdbiorcy) {

        System.out.println("Dzwonię z numeru: " + numerTelefonu + " \nDzwonię na numer: "+ numerOdbiorcy);
        Random random = new Random();
        boolean polacznie = random.nextBoolean();
        System.out.println("Udało się połączyć: " + polacznie);
        if(polacznie == true){
            int dlugoscRozmowy = random.nextInt(99)+1;
            System.out.println("Czas rozmowy to: " +dlugoscRozmowy+ " minut");
        }
    }

    @Override
    public void zadzwonNaNrAlarmowy() {

    }
}
