package pl.sda.zadanie4;

public class Klimatyzacja implements Chlodzi, Grzeje {

    private double temp = 20;

    @Override
    public double pobierzTemp() {
//        System.out.println("Temp klimatyzcji: " + temp);
        return temp;
    }

    @Override
    public void zwiekszTemp() {
        temp += 10;

    }

    @Override
    public void schlodz() {
        temp -= 10;

    }

    @Override
    public void wyswietlTemp() {
        System.out.printf("Aktuana temperatura w pomieszzeniu wynosi: %f\n",pobierzTemp());
    }
}
