package pl.sda.zadanie4;

public interface Grzeje {
    double pobierzTemp();
    void zwiekszTemp();

    default void wyswietlTemp(){
        System.out.printf("Aktuana temperatura w pomieszzeniu wynosi: %f\n",pobierzTemp());
    }
}
