package pl.sda.zadanie4;

public class Wiatrak implements Chlodzi {
    int temp = 100;

    @Override
    public double pobierzTemp() {
//        System.out.println("Temp wiatraka: " + temp);
        return temp;
    }

    @Override
    public void schlodz() {
        temp -= 10;

    }
}
