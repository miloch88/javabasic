package pl.sda.zadanie4;

public class Main {
    public static void main(String[] args) {

        Farelka farelka = new Farelka();
        Wiatrak wiatrak = new Wiatrak();
        Klimatyzacja klimatyzacja = new Klimatyzacja();

        wiatrak.schlodz();
        wiatrak.pobierzTemp();

        klimatyzacja.schlodz();
        klimatyzacja.schlodz();
        klimatyzacja.zwiekszTemp();
        klimatyzacja.pobierzTemp();

        farelka.zwiekszTemp();
        farelka.zwiekszTemp();
        farelka.pobierzTemp();

        klimatyzacja.wyswietlTemp();
        farelka.wyswietlTemp();
        wiatrak.wyswietlTemp();
    }
}
