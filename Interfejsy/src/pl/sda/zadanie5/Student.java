package pl.sda.zadanie5;

public class Student implements Comparable{

    String imie;
    String nazwisko;

    public Student(String imie, String nazwisko, int nrAlbumu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrAlbumu = nrAlbumu;
    }

    int nrAlbumu;

    @Override
    public int compareTo(Object o) {
//        Student that = (Student) o;
//        return Integer.compare(nrAlbumu,that.nrAlbumu);//nrAlbumu,that.nrAlbumu

        //Zwraca liczbę ujemną albo dodatnią
//        return this.nrAlbumu - ((Student)o).nrAlbumu;
        return ((Student)o).nrAlbumu - this.nrAlbumu;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d ", imie, nazwisko, nrAlbumu);
    }
}
