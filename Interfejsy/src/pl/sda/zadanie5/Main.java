package pl.sda.zadanie5;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Student studen1 = new Student("Kasia","Kasiowska",150);
        Student student2 = new Student("Basia","Basiowska",50);
        Student student3 = new Student("Kamil", "Kamilowski", 100);

        Student[] studenci = new Student[]{studen1,student2,student3};

        System.out.println(Arrays.toString(studenci));
        Arrays.sort(studenci);
        System.out.println(Arrays.toString(studenci));

    }
}
