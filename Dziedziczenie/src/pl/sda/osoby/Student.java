package pl.sda.osoby;

public class Student extends Osoba {

    private long numerIndeksu;

    public Student(String imie, String nazwisko, int wiek,long numerIndeksu) {
        super(imie, nazwisko, wiek);
        this.numerIndeksu=numerIndeksu;
    }

    @Override
    public void przedstawSie() {
        System.out.printf("Cześć, jestem %s i studiuję prawo\n", imie);
    }

    @Override
    public String toString() {
        String opis = String.format("%s, %s, nr albumu: %d", imie, nazwisko,numerIndeksu);
        return opis;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Student){//bolek
            Student ten = (Student) obj;//bolek
            if(this.numerIndeksu == ten.numerIndeksu){
                return true;
            }
        }
        return false;
    }

}
