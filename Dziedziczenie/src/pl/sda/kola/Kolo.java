package pl.sda.kola;

public class Kolo {
    double r;
    protected final double LICZBA_PI = 3.14;

    public Kolo(double r) {
        this.r = r;
    }

    public final double obliczPole(){
       return LICZBA_PI*r*r;
    }
}
