package pl.sda.samochody;

public class Main {
    public static void main(String[] args) {
        Samochod samochod = new Samochod("Czerwony","Opel",5);
        Kabriolet kabriolet = new Kabriolet("Czerwony","Opel",5);
        Samochod samochod2 = new Samochod("Czerwony","Opel",4);

        for (int i = 0; i < 200 ; i++) {
            samochod.przyspiesz();
            kabriolet.przyspiesz();
        }

        System.out.println();

        System.out.println(kabriolet.czyDachSchowany());
        kabriolet.schowajDach();
        kabriolet.czyDachSchowany();

        System.out.println();

        System.out.println(samochod.toString());
        System.out.println(kabriolet.toString());
        System.out.println();
        System.out.println(samochod.equals(kabriolet));
    }
}

