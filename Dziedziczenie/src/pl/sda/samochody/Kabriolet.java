package pl.sda.samochody;

public class Kabriolet extends Samochod {
    boolean dach;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    public void schowajDach() {
        dach = true;
    }

    @Override
    public void przyspiesz() {
        if (predkosc < 180) {
            predkosc += 20;
            System.out.printf("Kabriolet: Przyśpieszam do %d. km/h. \n", predkosc);
        }
    }

    public boolean czyDachSchowany() {
        if (dach) System.out.println("Dach jest schowany");
        return dach;
    }

//    @Override
//    public String toString() {
//        return String.format("%s samochód marki %s rocznik %d z rozsuwanym dachem",kolor,marka,rocznik);
//    }


    @Override
    public String toString() {
        return super.toString()+ "z rozsuwanym dachem";
    }
}
