package pl.sda.samochody;

public class Samochod {
    int predkosc;
    boolean swiatla;
    protected String kolor;
    protected String marka;
    protected int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz() {

        if (predkosc < 120) {
            predkosc += 10;
            System.out.printf("Samochód: Przyśpieszam do %d. km/h. \n", predkosc);
        }
    }

    public void wlaczSwiatla() {
        swiatla = true;
    }

    public boolean czySwiatlaSaWlaczone() {
        return swiatla;
    }

    @Override
    public String toString() {
        return String.format("%s samochód marki %s rocznik %d...", kolor, marka, rocznik);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod) {
            Samochod ten = (Samochod) obj;
            if (this.kolor == ten.kolor) {
                if (this.marka == ten.marka) {
                    if (this.rocznik == ten.rocznik) {return true;}
                }
            }
        }return false;
    }
}

