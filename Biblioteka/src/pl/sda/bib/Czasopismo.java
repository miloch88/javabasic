package pl.sda.bib;

public class Czasopismo extends Egzemplarz {
    private String nazwa;
    private int numer;

    public Czasopismo(String nazwa, int numer, Autor[] autorzy, int rokWydania, int iloscStron) {
        super(autorzy, rokWydania, iloscStron);
        this.nazwa = nazwa;
        this.numer = numer;
    }

    @Override
    public String pobierzTytul() {
        return String.format("%s %d", nazwa, numer);
    }
}
