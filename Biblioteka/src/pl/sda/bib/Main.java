package pl.sda.bib;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){

        Autor tolkien = new Autor("JRR", "Tolkien", "angielski");
        Autor sienkiewicz = new Autor("Henryk", "Sienkiewicz","polski");
        Autor prus = new Autor("Bolesław", "Prus", "polski");

        Autor[] pracaGrupowa = new Autor[] {sienkiewicz, prus};

        Ksiazka hobbit = new Ksiazka("Hobbit", new Autor[]{tolkien}, 2005, 300);
        Ksiazka lotr = new Ksiazka("Władca Pierścieni 1", new Autor[]{tolkien}, 2009, 300);
        Ksiazka lotr2 = new Ksiazka("Władca Pierścieni 2", new Autor[]{tolkien}, 2009, 300);
        Ksiazka lotr3 = new Ksiazka("Władca Pierścieni 3", new Autor[]{tolkien}, 2009, 300);

        Ksiazka krzyzacy = new Ksiazka("Krzyżacy", new Autor[]{sienkiewicz}, 1987, 200);
        Czasopismo ogrodnik = new Czasopismo("Ogrodnik", 54, pracaGrupowa, 2000, 30);

        Egzemplarz[] zbior = new Egzemplarz[]{hobbit, lotr, lotr2, lotr3, krzyzacy, ogrodnik};

        Biblioteka biblioteka = new Biblioteka(zbior);

        Egzemplarz[] znalezione = biblioteka.szukajPoTytule("Władca");
        System.out.println(Arrays.toString(znalezione));

        Egzemplarz[] znalezione1 = biblioteka.szukajPoAutorze("Prus");
        System.out.println(Arrays.toString(znalezione1));

        Egzemplarz[] znalezion2 = biblioteka.szukajPoWszystkim("1987");
        System.out.println(Arrays.toString(znalezion2));
    }
}
