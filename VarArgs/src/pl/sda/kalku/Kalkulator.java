package pl.sda.kalku;

public class Kalkulator {

    int dodaj(int... liczba) {
        int suma = 0;
        for (int a : liczba) {
            suma += a;
        }
        return suma;
    }

    int odejmi(int... liczba) {
        int roznica = liczba[0];
        for (int a : liczba) {
            roznica -= a;
        }
        return roznica;
    }

    int dzialanie(String symbol, int... liczba) {
        switch (symbol) {
            case "+":
                int dzialanie1 = 0;
                for (int a : liczba) {
                    dzialanie1 += a;
                }
                return dzialanie1;
            case "-":
                int dzialanie2 = liczba[0];
                for (int a: liczba){
                    dzialanie2-=a;
                }
                return dzialanie2;
        }
        return 0;
    }
}


